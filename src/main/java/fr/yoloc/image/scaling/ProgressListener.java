package fr.yoloc.image.scaling;

public interface ProgressListener {

	void notifyProgress(float fraction);
	
}
