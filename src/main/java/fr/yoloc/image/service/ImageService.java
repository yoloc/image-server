package fr.yoloc.image.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FilenameUtils;

import fr.yoloc.image.domain.MediaImage;
import fr.yoloc.image.scaling.ResampleOp;

@ApplicationScoped
public class ImageService {

	private File DEFAULT_IMAGE = null;

	@Inject
	@Named(value = "fileSystemStorageService")
	StorageService fileSystemStorageService;

	public File getImage(String source) {

		MediaImage mediaImage = new MediaImage(source);

		File image = fileSystemStorageService.read(mediaImage);

		// Vérification de l'existence de l'image.
		if (this.isExist(image)) {
			return image;
		}

		MediaImage mediaImageOriginal = (MediaImage) mediaImage.clone();
		
		mediaImageOriginal.changeDimension(DimensionList.ORIGINAL);

		File image_original = fileSystemStorageService.read(mediaImageOriginal);

		// Vérification de l'existence de l'image.
		if (this.isExist(image_original)) {
			mediaImageOriginal.setImage_original(image_original);
			Thread thread = new Thread(() -> resizeImage(mediaImage.getDimension(), mediaImageOriginal));
			thread.start();
			return image_original;
		}

		// Récupération de l'image par défaut
		return DEFAULT_IMAGE;
	}

	private boolean isExist(File fileName) {
		return fileName.exists();
	}


	private void resizeImage(DimensionList dimensionList, MediaImage mediaImage) {
		try {
			BufferedImage sourceImage = ImageIO.read(mediaImage.getImage_original());

			Dimension dimension = new Dimension(0, dimensionList.getHeight(), sourceImage.getWidth(), sourceImage.getHeight());

			ResampleOp resizeOp = new ResampleOp(dimension.width, dimension.height);
			BufferedImage resizedImage = resizeOp.filter(sourceImage, null);

			// Ecriture de l'image
			fileSystemStorageService.write(resizedImage, dimensionList, mediaImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static class Dimension {

		private int width;
		private int height;

		private Dimension(int dest_width, int dest_height, int orig_width, int orig_height) {
			if (dest_width < 1 && dest_height < 1) {
				this.width = 0;
				this.height = 0;
			} else if (dest_width < 1) {
				float height_ratio = (float) orig_height / (float) dest_height;
				this.height = dest_height;
				this.width = (int) (orig_width / height_ratio);
				System.out.println("ratio: " + height_ratio);
			} else if (dest_height < 1) {
				float width_ratio = (float) orig_width / (float) dest_width;
				this.width = dest_width;
				this.height = (int) (orig_height / width_ratio);
				System.out.println("ratio: " + width_ratio);
			} else {
				this.width = dest_width;
				this.height = dest_height;
			}
		}
	}

	public static enum DimensionList {

		SM("sm", 360), 
		MD("md", 640), 
		LG("lg", 1000), 
		ORIGINAL("", 0);

		public int getHeight() {
			return height;
		}

		private final String suffixe;

		private final int height;

		public static final String separator = "_";

		private DimensionList(String suffixe, int height) {
			this.suffixe = suffixe;
			this.height = height;
		}

		public String getSuffixe() {
			return this.suffixe;
		}

		public static DimensionList getInstance(String suffixe) {
			for (DimensionList dimension : DimensionList.values()) {
				if (dimension.suffixe.equals(suffixe)) {
					return dimension;
				}
			}
			return DimensionList.ORIGINAL;
		}

		public static DimensionList constructFileName(String filename) {
			String extension = FilenameUtils.getExtension(filename);
			String file_name_without_extension = filename.substring(0, filename.length() - extension.length() - 1);

			String[] tokens = file_name_without_extension.split(DimensionList.separator);

			if (tokens.length == 1) {
				// Fichier original
				return DimensionList.ORIGINAL;
			}
			return getInstance(tokens[tokens.length - 1]);
		}
	}

}
