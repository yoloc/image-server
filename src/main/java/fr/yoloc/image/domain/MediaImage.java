package fr.yoloc.image.domain;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.io.FilenameUtils;

import fr.yoloc.image.service.ImageService.DimensionList;

public class MediaImage implements Cloneable {

	private String extension;

	private DimensionList dimension;

	private String baseFileName;

	private File image_original;

	public void setImage_original(File image_original) {
		this.image_original = image_original;
	}

	private String fileName;

	private String baseFileNameWithExtension;

	private String fileNameWithoutExtension;

	public String getFileNameWithoutExtension() {
		return fileNameWithoutExtension;
	}

	public MediaImage(String srcFileName) {
		fileName = srcFileName;
		extension = FilenameUtils.getExtension(srcFileName);
		fileNameWithoutExtension = srcFileName.substring(0, srcFileName.length() - extension.length() - 1);

		String[] tokens = fileNameWithoutExtension.split(DimensionList.separator);

		if (tokens.length == 1) {
			// Fichier original
			dimension = DimensionList.ORIGINAL;
			baseFileName = tokens[0];
		} else {
			dimension = DimensionList.getInstance(tokens[tokens.length - 1]);
			if (!DimensionList.ORIGINAL.equals(dimension)) {
				tokens = Arrays.copyOfRange(tokens, 0, tokens.length - 1);
			}
			baseFileName = String.join(DimensionList.separator, tokens);
		}

		baseFileNameWithExtension = baseFileName + "." + extension;
	}

	public void changeDimension(DimensionList dimension) {
		this.dimension = dimension;
		if (!DimensionList.ORIGINAL.equals(dimension)) {
			this.fileNameWithoutExtension = baseFileName + DimensionList.separator + dimension.getSuffixe();
		} else {
			this.fileNameWithoutExtension = baseFileName;			
		}
		this.fileName = this.fileNameWithoutExtension + "." + this.extension;
	}
	
	public Object clone() {
		MediaImage mediaImage = null;
	    try {
	    	// On récupère l'instance à renvoyer par l'appel de la 
	      	// méthode super.clone()
	    	mediaImage = (MediaImage) super.clone();
	    } catch(CloneNotSupportedException cnse) {
	      	// Ne devrait jamais arriver car nous implémentons 
	      	// l'interface Cloneable
	      	cnse.printStackTrace(System.err);
	    }
	    
	    // on renvoie le clone
	    return mediaImage;
	}

	public String getBaseFileNameWithExtension() {
		return baseFileNameWithExtension;
	}

	public String getFileName() {
		return fileName;
	}

	public String getExtension() {
		return extension;
	}

	public DimensionList getDimension() {
		return dimension;
	}

	public String getBaseFileName() {
		return baseFileName;
	}

	public File getImage_original() {
		return image_original;
	}

}
